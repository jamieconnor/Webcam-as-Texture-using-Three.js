# Webcam as Texture using Three.js 
 
This html creates a 3d cube in three.js and uses the webcam video as the texture.
*I only made it work in chrome at the moment, and you need your webcam plugged in so it can connect.*


## Working demo of this page
https://seanwasere.gitlab.io/Webcam-as-Texture-using-Three.js/

## Video Tutorial on how to use this page with OBS
https://www.youtube.com/watch?v=SdYf4OvnVxY

## -- Update --
Added chroma option from the seriously.js library so chroma keying can also be applied at the webcam capture level.

*Settings shown work best for my situation. You will need to adjust for your own environment.*

![With CHROMA on and off](chromaOnOff.gif )

